import numpy as np
import matplotlib.pyplot as plt
from preprocessing import create_dataset
from sklearn import ensemble
from sklearn import neighbors, tree
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, cross_val_score


<<<<<<< HEAD

from sklearn.svm import LinearSVR


X_train, Y_train, X_test, Y_test = create_dataset()
=======
X_train, Y_train, X_test, Y_test = create_dataset(log=True)
>>>>>>> f6cf268e95ef0a6fb10921e0dcf9ec9db21116db
scores_on_train = {}
mse_on_test = {}
r2_on_test = {}


def parameter_tuning(reg, grid, how='grid_search', cv=5, verbose=3, n_jobs=3):
    if how == 'grid_search':
        search = GridSearchCV(reg, grid, scoring='neg_mean_squared_error', cv=cv, verbose=verbose, n_jobs=n_jobs)
    else:
        search = RandomizedSearchCV(reg, grid, n_iter=10, scoring='neg_mean_squared_error',
                                    cv=cv, verbose=verbose, random_state=42, n_jobs=n_jobs)
    search.fit(X_train, Y_train)
    print('cross validation score of ' + str(search.best_estimator_) + ': ' + str(search.best_score_))
    scores_on_train[str(search.best_estimator_)] = search.best_score_
    return search.best_estimator_


def show_one_parameter(algo, param, values, kwargs=None, cv=5, n_jobs=3):
    if not kwargs:
        kwargs = {}
    scores = []
    reg = None
    for value in values:
        kwargs[param] = value
        reg = algo(**kwargs)
        scores.append(cross_val_score(reg, X_train, Y_train, scoring='neg_mean_squared_error',
                                      cv=cv, n_jobs=n_jobs, verbose=3).mean())
    plt.figure()
    plt.title('Negative mean squared error of ' + str(reg) + ' with tuning of ' + str(param))
    plt.plot(values, scores)
    plt.xlabel(param)
    plt.ylabel('Negative MSE')
    plt.show()


def plot_results(reg):
    y_pred = reg.predict(X_test)
    mse = mean_squared_error(y_pred, Y_test)
    r2 = r2_score(y_pred, Y_test)
    print('scores on test set')
    print('MSE: ', mse)
    print('r2: ', r2)
    print()
    sort_ind = np.argsort(Y_test)
    y1, y2 = [], []
    for ind in sort_ind:
        y1.append(y_pred[ind])
        y2.append(Y_test[ind])
    x = range(len(y1))
    plt.figure()
    plt.title('Confrontation with ground truth ' + str(reg))
    plt.plot(x, y1, color='r', label='prediction')
    plt.plot(x, y2, color='b', label='ground truth')
    plt.xlabel('ordered samples')
    plt.ylabel('number of insult words')
    plt.show()
    mse_on_test[str(reg)] = mse
    r2_on_test[str(reg)] = r2


<<<<<<< HEAD
=======
def knn():
    algo = neighbors.KNeighborsRegressor
    reg = algo()
    reg = parameter_tuning(reg, {'n_neighbors': [2, 3, 4, 5]}, cv=3)
    #show_one_parameter(algo, 'n_neighbors', list(range(1, 10)), cv=3)
    plot_results(reg)
>>>>>>> f6cf268e95ef0a6fb10921e0dcf9ec9db21116db


def decision_tree():
    algo = tree.DecisionTreeRegressor
    reg = algo()
    # ccp_alpha: Complexity parameter used for Minimal Cost-Complexity Pruning
    reg = parameter_tuning(reg, {'max_depth': [25]}, cv=3)
    #show_one_parameter(algo, 'max_depth', [10, 15, 20, 25, 30, 40, 50, 70, 100], cv=3)
    #show_one_parameter(algo, 'ccp_alpha', [0.0001, 0.001, 0.01, 0.1], cv=3, kwargs={'max_depth': 30})
    plot_results(reg)


def random_forest():
    algo = RandomForestRegressor
    reg = algo()
    reg = parameter_tuning(reg, {'n_estimators': [20, 30], 'max_depth': [30, 50], 'n_jobs': [3]}, cv=3)
    show_one_parameter(algo, 'n_estimators', [10, 20, 30], cv=3, kwargs={'n_jobs': 3})
    show_one_parameter(algo, 'max_depth', [20, 30, 50, 80], cv=3, kwargs={'n_jobs': 3, 'n_estimators': 100})
    plot_results(reg)

def knn():
    algo = neighbors.KNeighborsRegressor
    reg = algo()
    reg = parameter_tuning(reg, {'n_neighbors': list(range(2, 4))}, cv=3)
    show_one_parameter(algo, 'n_neighbors', list(range(2, 5)), cv=3)
    plot_results(reg)
    
def svr():
    algo = LinearSVR
    reg = algo()
    param = {'C': [1], 'tol': [0.01],  'epsilon' : [1]}
    reg = parameter_tuning(reg, param, cv=3)
    show_one_parameter(algo, 'C', [1], cv=3)
    plot_results(reg)

def compare_scores():
    plt.figure()
    plt.title('Mean squared error obtained by cross validation on train set for every algorithms')
    plt.bar(*zip(*scores_on_train.items()))
    plt.show()

    plt.figure()
    plt.title('Mean squared error on test set for every algorithms')
    plt.bar(*zip(*mse_on_test.items()))
    plt.show()

    plt.figure()
    plt.title('Coefficient of determination on test set for every algorithms')
    plt.bar(*zip(*r2_on_test.items()))
    plt.show()

    
def gradient_boosting():
    #tuning parameters
    parametres= {"learning_rate":[0.3,0.2,0.1],"max_depth":[3,5,7],"subsample":[1.0,0.5]} #
    n_estimators = 100

    #creating model
    GBR = ensemble.GradientBoostingRegressor(n_estimators = n_estimators)

    #cross validation
    grille = GridSearchCV(estimator=GBR,param_grid=parametres)
    resultats= grille.fit(X_train,Y_train)

    # Check best model parameters
    print("The optimal parameters are : ", resultats.best_params_)
    # WARNING : Ces calculs sont longs et peuvent prendre jusqu'a 10min 

    print('Score of Gradient boosting Regressor on training set: {:.2f}'.format(resultats.score(X_train, Y_train)))
    print('Score of Gradient boosting Regressor on test set: {:.2f}'.format(resultats.score(X_test, Y_test)))


if __name__ == '__main__':
    svr()
    knn()
    decision_tree()
    random_forest()
    compare_scores()
    gradient_boosting()
