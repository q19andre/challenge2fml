import torch
import torch.nn as nn
import matplotlib.pyplot as plt
import pandas as pd
from torch.utils.data import Dataset, DataLoader
from preprocessing import create_dataset

X_train, y_train, X_test, y_test = create_dataset(plot=False)
y_train = pd.DataFrame(data=y_train, columns=["nbrMotInsulte"])
y_test = pd.DataFrame(data=y_test, columns=["nbrMotInsulte"])

#drop the maximum that is an outlier
index = y_test[y_test['nbrMotInsulte'] == 7227.0].index
y_test = y_test.drop(index)
X_test = X_test.drop(index)

class CustomDataset(Dataset):

    def __init__(self, df_x, df_y):
        """
        :param df_x: pandas.DataFrame that contains our input data
        :param df_y: pandas.DataFrame that contains our target data
        """
        self.df_x = df_x
        self.df_y = df_y

    def __len__(self):
        return len(self.df_x)

    def __getitem__(self, item):
        x_sample = self.df_x.iloc[item, :]
        nbMots = self.df_y.iloc[item, :]
        x_sample = torch.tensor(x_sample).float()
        nbMots = torch.tensor(nbMots).float()
        return x_sample, nbMots

train_dataset = CustomDataset(X_train, y_train)
test_dataset = CustomDataset(X_test, y_test)
train_loader = DataLoader(train_dataset, batch_size=20)
test_loader = DataLoader(test_dataset, batch_size=20)

class Net(nn.Module):

    # class initialization
    def __init__(self, input_size, hidden_size, output_size):
        super(Net, self).__init__()
        # fully connected layer with linear activation
        self.fc0 = nn.Linear(input_size, hidden_size)
        # ReLu activation
        self.relu = nn.ReLU()
        # fully connected layer with linear activation
        self.fc1 = nn.Linear(hidden_size, output_size)

    # function to apply the neural network
    def forward(self, x):
        out = self.fc0(x)
        out = self.relu(out)
        y_pred = self.fc1(out)
        return y_pred

# Create the neural network
# 10 neurons in the hidden layer, and 1 output size for y)
net = Net(len(X_train.columns), 10, 1)
net = net.float()
# Loss function: MSE = sum [(y - y_pred)^2]
criterion = nn.MSELoss()
optimizer = torch.optim.SGD(net.parameters(), lr = 0.0001)

def training():
    #train the model
    epochs = 5 # number of epochs
    # Loop on epochs
    for i in range(epochs):
        print(i/epochs)
        for i, data in enumerate(train_loader, 0):
            inputs, nbMots = data
            optimizer.zero_grad()
            output = net(inputs)
            loss = criterion(output, nbMots)
            loss.backward()
            optimizer.step()
    print('Finished training')

def testing():
    correct = 0
    total = 0
    residus = []
    with torch.no_grad():
        for inputs, nbMots in test_loader:
            output = net(inputs)
            for i in range(len(nbMots)):
                total += 1
                residus.append(abs(output[i][0] - nbMots[i][0]))
                if abs(output[i][0] - nbMots[i][0]) <= 20:
                    correct += 1

    print("Finished testing")
    plt.hist(residus, range=(0,150), bins=10)
    plt.xlabel('Difference between target and prediction')
    plt.ylabel('Number of samples')
    plt.show()
    print('Accuracy of the network : %d %%' % (100 * correct / total))

training()
testing()