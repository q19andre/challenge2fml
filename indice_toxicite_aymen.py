#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 16 23:04:04 2021

@author: aymenkallala
"""
from preprocessing import create_dataset
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestRegressor

X_train, Y_train, X_test, Y_test = create_dataset()

#Utilisons les fonctions de scikit et le random forest que nous avons implémenté afin d'extraire les features
#prépondérantes dans la régression du nombre de mots d'insultes d'une vidéo donnée et un certains poids 
#associé à ces features

def important_features():

    feature_names = [X_train.columns[i] for i in range(X_train.shape[1])]

    rfr = RandomForestRegressor(n_estimators=100)
    rfr.fit(X_train,Y_train)

    importances = rfr.feature_importances_
    std = np.std([tree.feature_importances_ for tree in rfr.estimators_], axis=0)

    forest_importances = pd.Series(importances, index=feature_names)

    fig, ax = plt.subplots()
    forest_importances.plot.bar(yerr=std, ax=ax)
    ax.set_title("Feature importances")
    ax.set_ylabel("Mean decrease in impurity")
    fig.tight_layout()
  
important_features()

    
#Maintenant que l'on a repérer les features pertinentes à exploiter, determinons un moyen de calculer
#la toxicité d'une video donnée en faisant simplement la somme et la pondération de ces features

def score_toxic(video):
    return(0.28*video[0] + 0.116*video[4] + 0.095 *video[6] + 0.12*video[12] + 0.195*video[13])

#Voici une répartition rapide des scores toxics de notre set d'entrainement 

def calcul_scores_toxic_train():
    
    scores = []
    for i in range(X_train.shape[0]):
        scores.append(score_toxic(X_train.iloc[i,:]))
    
    
    return(scores)

def calcul_scores_toxic_test():
    
    scores = []
    for i in range(X_test.shape[0]):
        scores.append(score_toxic(X_test.iloc[i,:]))
    
    return(scores)

def show_scores_toxic ():
    plt.plot(calcul_scores_toxic_train())
    
#nous allons déterminer le seuil de toxicité, à partir de maintenant une video avec un score_toxic >=1 sera
#considérée comme toxique

#Nous allons maintenant changer notre target donc les vecteurs Y_train et Y_test
# afin de déterminer si les vidéos de notre dataset sont toxiques ou pas.

def creer_nouvelle_target ():
    scores_train = calcul_scores_toxic_train()
    scores_test = calcul_scores_toxic_test()
    
    for i in range(len(scores_train)):
        if scores_train[i]>= 1:
           scores_train[i] = 1
        else:
           scores_train[i] = 0
        
    for i in range(len(scores_test)):
        if scores_test[i]>= 1:
           scores_test[i] = 1
        else:
           scores_test[i] = 0
        
        
    return (scores_train,scores_test)

train,test = creer_nouvelle_target()
Y_train = pd.DataFrame({'Toxique':train })
Y_test = pd.DataFrame({'Toxique':test })


#Maintenant que nous avons un nouveau dataset adapté a l'objectif qui est de créer un indice de toxicité.
#Entrainons un classifier afin de prédire si une vidéo est toxique ou pas.
def regression_logistique(): 
    from sklearn.linear_model import LogisticRegression

    lr = LogisticRegression(penalty='none')
    lr.fit(X_train,Y_train)

    import sklearn.metrics as metrics 

    #applied prediction
    pred = lr.predict(X_test)
    
   # print(lr.predict(np.array([[123,123,34,3,8,9,8,7,8,989,987876,3,193,387,83,7,83764,78,984,84,32,784,3398,3829,987,3928,7,8,9]])))


     #evaluation du modele
    print(metrics.classification_report(Y_test,pred))
    

regression_logistique()











