#import libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import StandardScaler


sns.set_style('darkgrid')
plt.rc('figure', figsize=[12, 9])
scaler = StandardScaler()
nbr_columns = ['nbrMotInsulte', 'nbrMot', 'nbrMotAllong', 'nbrMotMAJ', 'nbrExclMark', 'nbrQuestMark']
comment_columns = ['message_count', 'thread_count', 'discussion_count']
auth_columns = ['distinct_authors_count', 'authors_3channels_count', 'liked_authors_count']
mean_columns = ['nbrMotMoyenne', 'nbrMotInsulteMoyenne', 'nbrMotAllongMoyenne',
                'nbrMotMAJMoyenne', 'nbrExclMarkMoyenne', 'nbrQuestMarkMoyenne']
num_columns = nbr_columns + comment_columns + auth_columns + ['subscriberCount', 'viewCount']
cat_columns = ['categorie_new', 'categ_inst']


# outputs the following list: [X_train, y_train, X_test, y_test]
def create_dataset(plot=False, log=False):
    df = load_dataset()
    if log:
        df = modify_num_data(df, plot)
    if plot:
        describe_cat_data(df)
    df = one_hot_encoding(df)
    if plot:
        correlation_matrix(df)
    df2 = df.sample(frac=0.8, random_state=123)  # must set a seed value in random_state option
    test = df.drop(df2.index, axis=0)
    train = remove_outliers(df)
    if plot:
        find_outliers(df, train)
        describe_num_data(train)
    X_train, y_train = data_and_label(train)
    X_train = normalize(X_train)
    X_test, y_test = data_and_label(test)
    X_test = normalize(X_test, False)
    print()
    print('dataset created')
    print()
    return [X_train, y_train, X_test, y_test]


def load_dataset():
    df = pd.read_csv('Dataset/challenge_youtube_toxic.csv', encoding ='unicode_escape', sep=';', decimal=',')
    # channel name does not seem relevant and will create a lot of dimensions with the one hot encoding
    #TODO: maybe the combined variables are useless
    df = df.drop(['channel_name', 'video_id', 'video_id_court', 'channel_id', 'Unnamed: 27'], axis=1)
    return df


def describe_cat_data(df):
    mv = 'some' if df.isnull().values.any() else 'no'
    msg = 'We have ' + str(len(num_columns)) + ' numerical features and ' + str(len(cat_columns)) + \
          ' categorical features for ' + str(len(df.index)) + ' samples.\nThere are ' + mv + ' missing values in this dataset.\n'
    for c in cat_columns:
        plot_scatter(df, c)
        plot_categorical(df, c)


def describe_num_data(df):
    for c in num_columns:
        if c != 'nbrMotInsulte':
            plot_scatter(df, c)
        plot_hist(df, c)


def plot_scatter(df, feature):
    plt.figure()
    plt.title('Plot of number of insulting words by ' + feature)
    plt.scatter(df[feature], df['nbrMotInsulte'])
    plt.show()


def plot_hist(df, feature):
    plt.figure()
    plt.title('Plot of histogram of ' + feature)
    df[feature].hist()
    plt.show()


def plot_categorical(df, feature):
    counts = df[feature].value_counts(normalize=True)
    plt.figure()
    plt.title('Plot of the percentage of samples per distinct value of ' + feature)
    plt.bar(counts.index, counts, align='center', alpha=0.5)
    plt.xticks(counts.index, counts.index)
    plt.show()


def modify_num_data(df, plot):
    cols = nbr_columns + comment_columns + auth_columns
    df[cols] = df[cols].apply(lambda x: np.log(x+1))
    if plot:
        describe_num_data(df)
    return df


def find_outliers(df, df2):
    for c in num_columns:
        boxplot(df, df2, c)


def boxplot(df, df2, feature):
    plt.figure()
    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('Boxplots of ' + feature + ' for Youtube comments, with and without outliers')
    sns.boxplot(ax=ax1, data=df[feature], fliersize=10)  # fliersize is the size used to indicate the outliers
    sns.boxplot(ax=ax2, data=df2[feature], fliersize=10)
    plt.show()


def remove_outliers(df, log=False):

    for c in nbr_columns:
        if c != 'nbrMot':
            df = df[df[c] <= 2000]
            df = df[df[c] <= 30 * df['message_count']]
        else:
            df = df[df[c] <= 400000]
            df = df[df[c] <= 1000*df['message_count']]

    '''# for the number of words or messages, we just leave out the very isolated ones
    df = df[df['nbrMot'] <= 400000]
    df = df[df['message_count'] <= 8000]
    df = df[df['distinct_authors_count'] <= 7500]
    df = df[df['subscriberCount'] <= 400000]'''
    return df


def one_hot_encoding(df):
    one_hot = pd.get_dummies(df[cat_columns])
    df = pd.concat([df, one_hot], axis=1)
    df = df.drop(cat_columns, axis=1)
    return df


def correlation_matrix(df):
    corr = df.corr()
    # Generate a mask for the upper triangle
    mask = np.zeros_like(corr, dtype=bool)
    mask[np.triu_indices_from(mask)] = True
    # Generate a custom diverging colormap
    cmap = sns.diverging_palette(220, 10, as_cmap=True)
    plt.figure()
    plt.title('Correlation matrix for Youtube comments')
    sns.heatmap(corr, mask=mask, cmap=cmap)
    plt.show()


def data_and_label(df):
    y = df['nbrMotInsulte']
    y = y.to_numpy()
    X = df.drop(['nbrMotInsulte'], axis=1)
    return [X, y]


def normalize(data, train=True):
    if train:
        scaler.fit(data)
    return pd.DataFrame(scaler.transform(data), columns=data.columns)


if __name__ == '__main__':
    create_dataset(plot=True, log=True)
